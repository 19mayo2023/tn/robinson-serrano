/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package prueba_tecnica_algoritmo2;
import java.util.Arrays;
/**
 *
 * @author XEOZ
 */
public class Prueba_Tecnica_Algoritmo2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[] coordenadas = {1,2,-1,1,0,1,2,-1,-1,-2};
        
        int x = 0;
        int y = 0;
        
        //System.out.print("Prueba");
        
        //sumamos las cordenadas
        for (int i = 0; i < coordenadas.length; i+=2){
            //X son los impares
            x = x + coordenadas[i];
            //System.out.print(x);
            //Y son los pares
            y = y + coordenadas[i + 1];
            //System.out.print(y);
        }
        
        //cuando ya tenemos las cordenadas solo pintamos los datos cuando sean iguales
        for (int i = 0; i < 4;i++){
            for (int j= 0; j < 4 ;j++){
                if(i == x && j == y){
                    System.out.print("X");
                }else {
                    System.out.print("O");
                }

            }
            System.out.println();
        }
        
        
    }
    
}
