/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package prueba_tecnica_algoritmo1;

import java.util.Arrays;

/**
 *
 * @author XEOZ
 */
public class Prueba_Tecnica_Algoritmo1 {

    /**
     * @param args the command line arguments
     */
public static void main(String[] args) {
        int[] ar = { 1, 2, 2, 1, 1,1, 3, 5, 5, 5, 1, 2 };

        encontrarElementosRepetidos(ar);
       
    };



    private static void encontrarElementosRepetidos(int[] arOriginal) {

        int contador = 0;
        //se asigna en la variable aux el primer puesto del arreglo
        int aux = arOriginal[0];
        //aux2 llevara el numero
        int aux2 = arOriginal[0];
        
        //llevara las veces que se repite el numero
        int aux3 = 0;

        
        for (int i = 0; i < arOriginal.length; i++) {
            if (aux == arOriginal[i]) {
                //para calcular las veces que se repite 
                contador++;
                
                //verifica si es mayor
                if(contador > aux3 ){
                    aux3 = contador;
                    aux2 = arOriginal[i];
                }
                
                
            } else {
                //si no son iguales pasa al siguiente registro
                contador = 1;
                aux = arOriginal[i];
                
            }

        }
        System.out.print("Recurencias: " + aux3);
        System.out.print("\nNumero: " + aux2 +"\n");

    }
    
}
